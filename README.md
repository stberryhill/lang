# Lang
Mainly just a tiny collection of typedefs that make C89 better.

Also includes an unambiguous string comparison function (strequ) which is
missing in C89.

by S. Tyler Berryhill
