#include "strings.h"
#include <string.h>

bool strequ(const char *a, const char *b) {
  return (strcmp(a, b) == 0);
}
