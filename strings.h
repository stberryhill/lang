#ifndef _TYLER_STRINGS_H
#define _TYLER_STRINGS_H

#include "types.h"

bool strequ(const char *a, const char *b);

#endif
