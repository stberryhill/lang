#ifndef _TYLER_HELPFUL_TYPES_H
#define _TYLER_HELPFUL_TYPES_H

#include <limits.h>

#define true  1
#define false 0

typedef unsigned char  bool;
typedef unsigned char  uint8;
typedef unsigned short uint16;
typedef unsigned long  uint32; /* TODO use target find better match */
typedef unsigned int   uint;
typedef unsigned int   uint64;

typedef char  int8;
typedef short int16;
typedef int   int64;

/* info in limits.h */
#define TARGET_MAX 2147483647L

#if   SCHAR_MAX >= TARGET_MAX
  typedef signed char int32;
#elif SHORT_MAX >= TARGET_MAX
  typedef short int32;
#elif INT_MAX   >= TARGET_MAX
  typedef int int32;
#else
  typedef long int32;
#endif

#undef TARGET_MAX

#endif
